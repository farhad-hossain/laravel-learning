<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
});
Route::get('/register', function () {
    return view('register');
});
Route:: get("/employee_list","LoginController@show");
Route:: get("/employee_delete/{id}","LoginController@delete");
Route:: post("login-post","LoginController@login");
Route:: post("/register-post","LoginController@store");
