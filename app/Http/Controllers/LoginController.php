<?php

namespace App\Http\Controllers;
use App\Login;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        //
    }

    public function login(Request $login_request)
    {
        dd($login_request);

    }

    public function store(Request $request)
    {
        Login::create($request->all());

        return redirect('register');

    }

    public function show()
    {
        $lists = Login::all();
        return view('employee_list', compact('lists'));
    }
    public function delete($id){
        Login::destroy($id);
        return redirect()->back();
    }

    public function edit(Login $login)
    {

    }

    public function update(Request $request, Login $login)
    {
        //
    }

    public function destroy(Login $login)
    {
        //
    }
}
