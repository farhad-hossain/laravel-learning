
<a href="{{ url('/register') }}">Register</a>
    <table class="table">
        <tr>
            <th>SL</th>
            <th>Name</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
@foreach($lists as $list)
    <tr>
        <th>{{ $loop->iteration }}</th>
        <th>{{ $list->name }}</th>
        <th>{{ $list->email }}</th>
        <th>
            <a href="{{ url('/') }}">Edit</a> |
            <a href="{{ url('/employee_delete/'.$list->id) }}">Delete</a>
        </th>
    </tr>
    @endforeach
    </table>